variable "rg_name" {
    type = string
    description = "Variable to set name of resource group"
}
variable "location" {
    type = string
    description = "Variable to set location for a resource"
    default = "West US"
}
